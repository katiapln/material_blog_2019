<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\UsersControleur;
use \App\Modeles\UsersModele AS User;


function dashboardAction(\PDO $connexion) {
  GLOBAL $content1, $title;
  $title = USERS_DASHBOARD_TITLE;
  ob_start();
    include '../app/vues/users/dashboard.php';
  $content1 = ob_get_clean();
}
