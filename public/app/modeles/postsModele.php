<?php
/*
    ./app/modeles/postsModele.php
 */
namespace App\Modeles\PostsModele;

function findAll(\PDO $connexion){
  $sql = "SELECT *
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          ORDER BY datePublication DESC
          LIMIT 5;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
